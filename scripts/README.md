## Scripts

I'm not sure these still work since I massively changed the way styles(colors are applied.

- `styles.py` will generate CSS stylesheets that are required for this plugin to actually colorize the code.  
  It has a help option: `python3 styles.py -h`  
  Also see `PicoPygments.stylesheet` in the `picopygments.yml` configuration file.
- `pygsgallery.py` will output an HTML fragment that contains sample code styled in all available styles,
  and a table of contents. If the file `pygsgallery.py.css` is found in the same folder, it will be used to style
  the HTML.  
  Save it to a file: `python3 pygsgallery.py > fragment.html`
