#!/bin/bash

out='<div class="all"><link rel="stylesheet" href="/plugins/PicoPygments/pygsgallery.sh.css" type="text/css" /><link rel="stylesheet" href="/plugins/PicoPygments/css/_common.css" type="text/css" />'
toc='<input id="dd" type="checkbox"/><label class=boxed for="dd">TOC</label><div class="boxed ddc">'
post='</div>'

for file in css/custom*.css; do
    name="${file##*/}"
    name="${name%.*}"
    name="${name#custom-}"
    mapfile -s1 style < "$file"
    style=("${style[@]//.pcpg/.pcpg.$name}")
    out="$out<div><style>${style[@]}</style><h3 id=\"$name\">$name<a href=\"#\"></a></h3>"
    code="<pre class=\"pcpg $name\"><code class=\"pcpg $name\">"
    while read -r class text; do
        ## "hll" means highlighted (current) line
        code="$code<span class=\"$class\">$text</span>\t"
    done < pygsgallery.classes
    out="$out$code</code></pre></div>"
    toc="$toc<a href=\"#$name\">$name</a>"
>&2 echo "Processing $name"
done

echo -e "$toc</div>$out$post"
