#!/usr/bin/python3

from pygments.lexers import guess_lexer, guess_lexer_for_filename

sep="---------------------------------------------------\n"

print("Guesses which language code is written in.\nPaste or type the raw code you want guessed (Ctrl-D when finished):")
lines = []
while True:
    try:
        line = input()
    except EOFError:
        break
    lines.append(line)
code='\n'.join(lines)

line=input(sep+"Enter potential filename, or just Enter if unknown: ")
if(line != ""):
    lexer=guess_lexer_for_filename(line,code)
else:
    lexer=guess_lexer(code)

print(sep+"Lexer:\t"+str(lexer)+"\nAliases:\t"+str(lexer.aliases)+"\nFilenames:\t"+str(lexer.filenames)+"\nMimetypes:\t"+str(lexer.mimetypes))
